package com.kotlin.uentral.base

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

//        //Esto me quita la barra de notificacion
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//            WindowManager.LayoutParams.FLAG_FULLSCREEN)

        //Con esto seteo el layout que voy a utilizar
        setContentView(getLayout())

    }

    //Con este metodo obtengo la direccion de memoria del layout
    @LayoutRes
    abstract fun getLayout(): Int

    //Funcion con la que puedo mostrar un Toast
    fun createToast(message: String){
        return Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun navigateTo(intent: Intent) {
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK// Esta bandera va a matar cualquier tarea a la hora de navegar aotra view
        startActivity(intent)
    }

}
