package com.kotlin.uentral

import android.app.Application

import com.kotlin.uentral.presentation.injection.DaggerPresentationComponent
import com.kotlin.uentral.presentation.injection.PresentationComponent
import com.kotlin.uentral.presentation.injection.PresentationModule

class UentralApp : Application() {


    private var appComponent: PresentationComponent? = null

    override fun onCreate() {
        super.onCreate()

        // Esta seria la inicializacion de todos los componentes que nuestra app necesita al correr la app
        appComponent =
            DaggerPresentationComponent.builder().presentationModule(PresentationModule()).build()
    }

    fun getAppComponent(): PresentationComponent? = appComponent
}