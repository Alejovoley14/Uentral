package com.kotlin.uentral.presentation.main.home.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import com.kotlin.uentral.R
import com.kotlin.uentral.base.BaseActivity
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState
import kotlinx.android.synthetic.main.home_layout.*


class HomeActivity : BaseActivity() {

    private lateinit var slideLayout: SlidingUpPanelLayout
    private lateinit var dragArea: ImageButton
    private lateinit var layoutIcons: ConstraintLayout
    private lateinit var slideUpLayout: ConstraintLayout

    override fun getLayout(): Int {
        return R.layout.home_layout
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        dragArea = findViewById(R.id.dragBtn)
        slideUpLayout = findViewById(R.id.slideUpLayout)
        slideLayout = findViewById(R.id.sliding_layout)
//        slideLayout.setDragView(dragArea)

        val inflater : LayoutInflater = LayoutInflater.from(this)

        sliding_layout.addPanelSlideListener(object :
            PanelSlideListener {

            override fun onPanelSlide(
                panel: View,
                slideOffset: Float
            ) {

            }

            override fun onPanelStateChanged(
                panel: View,
                previousState: PanelState,
                newState: PanelState
            ) {
                if ((newState == PanelState.COLLAPSED && previousState == PanelState.DRAGGING) ||(newState == PanelState.DRAGGING && previousState == PanelState.EXPANDED)) {
//                    dragArea.setImageResource(R.drawable.style_rectangle)
//                    dragArea.setBackgroundColor(Color.parseColor("#FFFFFF"))
                    slideUpLayout = inflater.inflate(R.layout.slide_collapsed, slideUpLayout, true) as ConstraintLayout

                }

                if ((newState == PanelState.EXPANDED && previousState == PanelState.DRAGGING) || (newState == PanelState.DRAGGING && previousState == PanelState.COLLAPSED)) {
//                    dragArea.setImageResource(R.drawable.style_rectangle_white)
//                    dragArea.setBackgroundColor(Color.parseColor("#1599eb"))
                    slideUpLayout = inflater.inflate(R.layout.slide_expanded, slideUpLayout, true) as ConstraintLayout
                }
//                createToast("Nuevo estado: ${newState} , Estado anterior: ${previousState}")
            }
        })

    }

}


