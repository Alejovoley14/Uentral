package com.kotlin.uentral.presentation.authentication.logIn.presenter

import android.util.Patterns
import com.kotlin.uentral.data.Usuario
import com.kotlin.uentral.domain.authentication.LogIn.LogInInteractor
import com.kotlin.uentral.presentation.authentication.logIn.LogInContratc
import com.kotlin.uentral.presentation.authentication.logIn.exception.LogInException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.regex.Pattern
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class LogInPresenter @Inject constructor(private val logInInteractor: LogInInteractor) :
    LogInContratc.Presenter, CoroutineScope {

    private var view: LogInContratc.View? = null

    private var job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun attachView(view: LogInContratc.View) {
        this.view = view
    }

    override fun dettachView() {
        view = null
    }

    override fun dettachJob() {
    }

    override fun isViewAttach(): Boolean {
        return view != null
    }

    override fun logInUser(usuario: Usuario) {
        launch {

            val patternEmail = Patterns.EMAIL_ADDRESS

            try {
                if (usuario.email.isEmpty()) {
                    view?.showError("Ingrese su email")
                } else {

                    if (!patternEmail.matcher(usuario.email).matches()) {
                        view?.showError("Ingrese su email de forma correcta")
                    }
                    else {

                        if (usuario.pass.isEmpty()) {
                            view?.showError("Ingrese su contraseña")
                        }
                        else {
                            view?.showProgessBar()
                            logInInteractor.logInUser(usuario)
                            view?.hideProgressBar()
                            view?.navigateToHome()
                        }
                    }
                }
            } catch (ex: LogInException) {
                view?.hideProgressBar()
                view?.showError(ex.message.toString())
            }
        }

    }

}