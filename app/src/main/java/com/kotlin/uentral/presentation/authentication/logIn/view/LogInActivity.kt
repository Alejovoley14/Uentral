package com.kotlin.uentral.presentation.authentication.logIn.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import com.google.android.material.textfield.TextInputEditText
import com.kotlin.uentral.R
import com.kotlin.uentral.UentralApp
import com.kotlin.uentral.base.BaseActivity
import com.kotlin.uentral.data.Usuario
import com.kotlin.uentral.presentation.authentication.logIn.LogInContratc
import com.kotlin.uentral.presentation.authentication.logIn.presenter.LogInPresenter
import com.kotlin.uentral.presentation.authentication.signIn.view.SignInActivity
import com.kotlin.uentral.presentation.main.home.view.HomeActivity
import kotlinx.android.synthetic.main.auth_login.*
import javax.inject.Inject

class LogInActivity : BaseActivity(), LogInContratc.View {

    private lateinit var emailEditText: EditText
    private lateinit var passwordEditText: TextInputEditText
    private lateinit var progressBar: ProgressBar

    @Inject
    lateinit var presenter: LogInPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Injecto dagger
        (application as UentralApp).getAppComponent()?.inject(this)

        emailEditText = findViewById(R.id.emailTxt)
        passwordEditText = findViewById(R.id.passEditTxt)
        progressBar = findViewById(R.id.progressBar)

        presenter.attachView(this)

        loginButton.setOnClickListener {

            val usuario = Usuario(
                email = emailEditText.text.toString(),
                pass = passwordEditText.text.toString(),
                apellido = null,
                nombre = null
            )

            presenter.logInUser(usuario)
        }

        signInTxt.setOnClickListener {
            navigateToSignIn()
        }
    }

    override fun getLayout(): Int {
        return R.layout.auth_login
    }

    override fun navigateToHome() {
        navigateTo(Intent(this, HomeActivity::class.java ))
    }

    override fun navigateToSignIn() {
        navigateTo(Intent(this, SignInActivity::class.java))
    }


    override fun showProgessBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun showError(errorMsg: String) {
        createToast(errorMsg)
    }
}
