package com.kotlin.uentral.presentation.injection

import com.kotlin.uentral.domain.authentication.LogIn.LogInInteractor
import com.kotlin.uentral.domain.authentication.LogIn.LogInInteractorImpl
import dagger.Module
import dagger.Provides

@Module
class PresentationModule {

    @Provides
    fun providesLogIn(): LogInInteractor = LogInInteractorImpl()

}