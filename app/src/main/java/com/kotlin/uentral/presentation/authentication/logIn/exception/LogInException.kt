package com.kotlin.uentral.presentation.authentication.logIn.exception

class LogInException(message: String) : Exception(message)