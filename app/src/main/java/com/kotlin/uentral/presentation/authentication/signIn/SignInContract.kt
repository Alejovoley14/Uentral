package com.kotlin.uentral.presentation.authentication.signIn

interface SignInContract {

    interface View {
        fun navigateToNextActivity()
    }
}