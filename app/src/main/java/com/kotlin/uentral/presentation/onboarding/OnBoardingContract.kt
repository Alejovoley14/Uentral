package com.kotlin.uentral.presentation.onboarding

interface OnBoardingContract {

    interface View{
        fun navigateToLogIn()
    }
}