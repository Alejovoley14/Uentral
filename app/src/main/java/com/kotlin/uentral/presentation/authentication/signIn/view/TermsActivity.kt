package com.kotlin.uentral.presentation.authentication.signIn.view

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Button
import com.kotlin.uentral.R
import com.kotlin.uentral.base.BaseActivity
import kotlinx.android.synthetic.main.terms_main.*

class TermsActivity : BaseActivity(){

    override fun getLayout(): Int {
        return R.layout.terms_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cancelTerms.setOnClickListener{

            val view = layoutInflater.inflate(R.layout.terms_decline_dialog, null)
            val alert : AlertDialog.Builder = AlertDialog.Builder(this)
            alert.setView(view)

            val alertDialog: AlertDialog = alert.create()
            alertDialog.setCanceledOnTouchOutside(false)

           val cancelBtn: Button = view.findViewById(R.id.backBtn)

            cancelBtn.setOnClickListener(){
                alertDialog.dismiss()
            }

            alertDialog.show()

        }
    }

}
