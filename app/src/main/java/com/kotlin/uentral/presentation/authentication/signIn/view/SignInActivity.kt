package com.kotlin.uentral.presentation.authentication.signIn.view

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import com.hbb20.CountryCodePicker
import com.kotlin.uentral.R
import com.kotlin.uentral.base.BaseActivity
import com.kotlin.uentral.presentation.authentication.signIn.SignInContract
import kotlinx.android.synthetic.main.auth_signin.*

class SignInActivity : BaseActivity(), SignInContract.View{

    private lateinit var cpp: CountryCodePicker

    override fun getLayout(): Int {
        return R.layout.auth_signin
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cpp = findViewById(R.id.numeroPaisTxt)

        val typeFace= Typeface.createFromAsset(this.assets,"inter_regular.ttf")
        cpp.setTypeFace(typeFace)

        siguienteBtn.setOnClickListener(){
            navigateToNextActivity()
        }

    }

    override fun navigateToNextActivity() {
        navigateTo(Intent(this, TermsActivity::class.java))
    }

}
