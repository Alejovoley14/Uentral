package com.kotlin.uentral.presentation.injection

import com.kotlin.uentral.presentation.authentication.logIn.view.LogInActivity
import dagger.Component
import javax.inject.Singleton

// Con esto se le indica que se va usar este modulo
@Component(modules = [PresentationModule::class])

@Singleton
interface PresentationComponent {

    fun inject(loginActivity: LogInActivity)
//    fun inject (registerActivity: RegisterActivity)
//    fun inject (forgotPassActivity: ForgotPassActivity)

}