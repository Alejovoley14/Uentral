package com.kotlin.uentral.presentation.onboarding.model

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import com.kotlin.uentral.R
import com.kotlin.uentral.presentation.authentication.logIn.view.LogInActivity

class PrefManager(var con: Context) {

    lateinit var  pref: SharedPreferences

    init {
        getSp()
    }

    private fun getSp() {
        pref = con.getSharedPreferences(con.getString(R.string.pref_name), Context.MODE_PRIVATE)
    }

    @SuppressLint("CommitPrefEdits")
    fun writeSp()
    {
        val editor: SharedPreferences.Editor = pref.edit()
        editor.putString(con.getString(R.string.pref_key), "Next")
        editor.apply()
    }

    fun checkPreferences() : Boolean {
        return !pref.getString(con.getString(R.string.pref_key), "null").equals("null")
    }

    fun clearPreferences(){
        pref.edit().clear().apply()

        con.startActivity(Intent(con, LogInActivity::class.java))
        (con as AppCompatActivity).finish()
    }
}