package com.kotlin.uentral.presentation.authentication.logIn

import com.kotlin.uentral.data.Usuario

interface LogInContratc {

    interface View {
        fun navigateToHome()
        fun navigateToSignIn()
        fun showProgessBar()
        fun hideProgressBar()
        fun showError(errorMsg: String)
    }

    interface Presenter {
        fun attachView(view: View)
        fun dettachView()
        fun dettachJob()
        fun isViewAttach(): Boolean
        fun logInUser(usuario: Usuario)
    }
}