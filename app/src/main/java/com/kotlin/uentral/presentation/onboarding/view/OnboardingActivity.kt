package com.kotlin.uentral.presentation.onboarding.view


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.kotlin.uentral.R
import com.kotlin.uentral.base.BaseActivity
import com.kotlin.uentral.presentation.authentication.logIn.view.LogInActivity
import com.kotlin.uentral.presentation.onboarding.OnBoardingContract
import com.kotlin.uentral.presentation.onboarding.model.PageAdapter
import com.kotlin.uentral.presentation.onboarding.model.PrefManager

class OnboardingActivity : BaseActivity(), OnBoardingContract.View {

    private lateinit var pager: ViewPager
    private var layouts : IntArray = intArrayOf(R.layout.onboarding_first, R.layout.onboarding_second, R.layout.onboarding_third)
    lateinit var pageAdapter: PageAdapter
    lateinit var dotsLayout: LinearLayout
    lateinit var dots: Array<ImageView>
    lateinit var btnSkip : Button
    lateinit var btnNext : Button

    override fun getLayout(): Int {
        return R.layout.onboarding_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (PrefManager(this).checkPreferences()){
            navigateToLogIn()
        }

        pager = findViewById(R.id.pager)
        pageAdapter = PageAdapter(layouts, this)
        pager.adapter = pageAdapter
        dotsLayout = findViewById(R.id.dots)
        btnNext = findViewById(R.id.btnNext)
        btnSkip = findViewById(R.id.btnSkip)


        btnNext.setOnClickListener{
            loadNextSlide()
        }

        createDots(0)

        pager.addOnPageChangeListener( object : ViewPager.OnPageChangeListener{

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {

               createDots(position)

                if (position != layouts.lastIndex) {
                    btnNext.text = "Finalizar"
                    btnNext.visibility = View.INVISIBLE
                }
                else{
                    btnNext.text = "Next"
                    btnNext.visibility = View.VISIBLE
                }
            }

        })

    }


    private fun loadNextSlide() {

        val nextSlide: Int = pager.currentItem + 1

        if (nextSlide < layouts.size){

            pager.currentItem = nextSlide
        }
        else{
            navigateToLogIn()
            PrefManager(this).writeSp()
        }

    }

    fun createDots(position: Int) {

        dotsLayout.removeAllViews()

        dots = Array(layouts.size) {ImageView(this)}

        for (i in layouts.indices){

            dots[i] = ImageView(this)

            if (i == position){

                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.style_active_dots))
            }
            else
            {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.style_inactive_dots))
            }

            val params :LinearLayout.LayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

            params.setMargins(4, 0, 4, 0)
            dotsLayout.addView(dots[i], params)
        }
    }

    override fun navigateToLogIn() {
        navigateTo(Intent(this, LogInActivity::class.java))
        finish()
    }


}






