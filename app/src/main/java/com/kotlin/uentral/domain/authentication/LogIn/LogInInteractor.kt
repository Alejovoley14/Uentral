package com.kotlin.uentral.domain.authentication.LogIn

import com.kotlin.uentral.data.Usuario

interface LogInInteractor {

    suspend fun logInUser(usuario: Usuario)
}