package com.kotlin.uentral.domain.authentication.LogIn

import com.kotlin.uentral.data.Usuario
import com.kotlin.uentral.presentation.authentication.logIn.exception.LogInException
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.suspendAtomicCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class LogInInteractorImpl : LogInInteractor {

    @InternalCoroutinesApi
    override suspend fun logInUser(usuario: Usuario): Unit = suspendAtomicCancellableCoroutine {

            corutine ->

        if (usuario.email == "alejo@gmail.com" && usuario.pass == "alejo1234") {
            corutine.resume(Unit)
        } else {
            corutine.resumeWithException(LogInException("Error al iniciar sesion"))
        }
    }

}
